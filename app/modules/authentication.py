from flask import Blueprint, g, Response, request, url_for, abort
from passlib.apps import custom_app_context as pwd_context
from functools import wraps
import arrow

from MAPI import *
from MAPI.Struct import *
from MAPI.Util import *
from MAPI.Time import *
import json
from pprint import pprint


'''
Authentication utilities and endpoints.
'''
mod = Blueprint('authentication', __name__)

@mod.route('/api/user/', methods=['POST'])
def get_user():
    username = request.json.get('username')
    password = request.json.get('password')
    isLogin = authenticate(username, password)
    return json.dumps({"isLogin" : isLogin})

def authenticate(username, password):
    try:
        session = OpenECSession(username, "", 'file:///var/run/zarafa')
    except MAPIErrorLogonFailed:
        return 'false'
    return 'true'