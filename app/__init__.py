from flask import Flask

app = Flask(__name__)

from modules import serve_static
app.register_blueprint(serve_static.mod, url_prefix='/')

from modules import authentication
app.register_blueprint(authentication.mod)