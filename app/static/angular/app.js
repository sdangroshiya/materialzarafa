var myApp = angular.module('StarterApp', ['ngRoute', 'ngMaterial']);
myApp.config(function($routeProvider, $mdThemingProvider){

     $mdThemingProvider.theme('Blue')
        .primaryPalette('indigo', {
            'default': '500', // by default use shade 900 from the grey palette for primary intentions

        });
 
    $mdThemingProvider.setDefaultTheme('Blue');

	$routeProvider.when('/', {
		controller : 'AppCtrl',
		templateUrl : 'angular/views/login.html'
	})
	.when('/register',{
		templateUrl: 'angular/views/inbox.html',
        controller: 'AppCtrl'
    });
})